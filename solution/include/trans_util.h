#ifndef __TRNAS_UTIL_H__
#define __TRNAS_UTIL_H__

#include "image.h"

void header_copy( const struct image* source, struct image* target );
void header_rotate( const struct image* source, struct image* target );

#endif
