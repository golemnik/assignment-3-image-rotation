#ifndef __IMAGE_ALLOCATOR_H__
#define __IMAGE_ALLOCATOR_H__

#include "image.h"

#include <stdlib.h>

enum alloc_status {
    ALLOC_OK,
    ALLOC_ERROR
};

enum alloc_status alloc_image( struct image* img );
enum alloc_status alloc_row( const size_t size, void** row );

enum free_status {
    FREE_OK,
    FREE_ERROR
};

enum free_status free_image( const struct image* img );
enum free_status free_row( void* row );

#endif
