#ifndef __TRANSFORMER_H__
#define __TRANSFORMER_H__

#include "image.h"

enum transform_status {
    TRANSFORM_OK = 0,
    TRANSFORM_NO_MEM,
    TRANSFORM_UNKNOWN,
    TRANSFORM_ERROR
};

enum transform_status transform(const struct image* source, struct image* target, const char* transformation);

#endif
