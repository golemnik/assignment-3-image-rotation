#ifndef __TRANS_ROTATE_H__
#define __TRANS_ROTATE_H__

#include "image.h"
#include "transformer.h"

enum transform_status rotate_0( const struct image* source, struct image* target );
enum transform_status rotate_90( const struct image* source, struct image* target );
enum transform_status rotate_180( const struct image* source, struct image* target );
enum transform_status rotate_270( const struct image* source, struct image* target );

#endif

