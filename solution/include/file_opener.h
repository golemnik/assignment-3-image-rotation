#ifndef __FILE_OPENER_H__
#define __FILE_OPENER_H__

#include <stdio.h>

enum open_mode {
    OPEN_FOR_READING,
    OPEN_FOR_WRITING
};

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum open_status open_file( const char* file_name, FILE** file, const enum open_mode mode );

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status close_file( FILE* file );

#endif
