#ifndef __IMAGE_H__
#define __IMAGE_H__

#include  <stdint.h>

#define BYTES_PER_PIXEL 3

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  uint32_t x_resolution, y_resolution;
  struct pixel* data;
};

#endif
