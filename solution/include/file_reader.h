#ifndef __FILE_READER_H__
#define __FILE_READER_H__

#include "image.h"

#include <stdio.h>

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_DATA_OFFSET,
  READ_NO_MEM,
  READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_INVALID_DIMENSION,
  WRITE_TOO_LONG_FILE,
  WRITE_NO_MEM,
  WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );


#endif
