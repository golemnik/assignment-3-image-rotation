#include "image.h"
#include "image_allocator.h"
#include "utils.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

enum alloc_status alloc_image( struct image* img )
{
    void* buf = malloc(img->width * img->height * sizeof(struct pixel));
    if (NULL == buf) {
        print_error( "ERRNO: %d %s\n", errno, strerror(errno));
        return ALLOC_ERROR;
    }
    img->data = buf;
    return ALLOC_OK;
}

enum alloc_status alloc_row( const size_t size, void** row )
{
    void* buf = malloc(size);
    if (NULL == buf) {
        print_error( "ERRNO: %d %s\n", errno, strerror(errno));
        return ALLOC_ERROR;
    }
    *row = buf;
    return ALLOC_OK;
}


enum free_status free_image( const struct image* img )
{
    free(img->data);
    return FREE_OK;
}

enum free_status free_row( void* row )
{
    free(row);
    return FREE_OK;
}

