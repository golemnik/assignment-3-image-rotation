#include "utils.h"

#include <stdarg.h>
#include <stdio.h>

void print_error(const char* msg, ... ) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args); // NOLINT
    va_end (args);
}

