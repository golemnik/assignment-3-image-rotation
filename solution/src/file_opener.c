#include "file_opener.h"
#include "utils.h"

#include <errno.h>
#include <string.h>

enum open_status open_file( const char* file_name, FILE** file, const enum open_mode mode )
{
    FILE* f = fopen(file_name, OPEN_FOR_READING == mode ? "r+b" : "w+b");
    if (NULL == f) {
        print_error("ERRNO: %d %s\n", errno, strerror(errno));
        return OPEN_ERROR; // TODO: can be related to errno
    }
    *file = f;
    return OPEN_OK;
}


enum close_status close_file( FILE* file )
{
    if (0 != fclose(file)) {
        print_error("ERRNO: %d %s\n", errno, strerror(errno));
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
