#include "trans_util.h"

void header_copy( const struct image* source, struct image* target )
{
    target->width = source->width;
    target->height = source->height;
    target->x_resolution = source->x_resolution;
    target->y_resolution = source->y_resolution;
}

void header_rotate( const struct image* source, struct image* target )
{
    target->width = source->height;
    target->height = source->width;
    target->x_resolution = source->y_resolution;
    target->y_resolution = source->x_resolution;
}
